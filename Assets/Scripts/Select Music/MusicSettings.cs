﻿using UnityEngine;
using UnityEngine.UI;

public class MusicSettings : MonoBehaviour
{
    public static float DecisionTimeOffset;
    public static int MusicIndex = 0;
    public static float Speed;

    public AudioSource musicPreview;
    public Slider sliderSpeed;

    public Text textSpeed;


    private int m_StereoPanLevel;

    private void Start()
    {
        Speed = 1f;
    }



    public void ChangeSpeed()
    {
        Speed = sliderSpeed.value;
        textSpeed.text = "Level " + sliderSpeed.value.ToString();
    }


}